<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Mail
 *
 * @property string $id
 * @property int    $user_id
 * @property string $internalDate
 * @property string $labels
 * @property string $headers
 * @property string $subject
 * @property string $from
 * @property string $fromName
 * @property string $fromEmail
 * @property string $to
 * @property string $deliveredTo
 * @property string $plainTextBody
 * @property string $rawPlainTextBody
 * @property int    $attachments
 *
 * @property string $created_at
 * @property string $modified_at
 *
 * @package App
 */
class Mail extends Model {

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return int
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * @return string
	 */
	public function getInternalDate()
	{
		return $this->internalDate;
	}

	/**
	 * @return string
	 */
	public function getLabels()
	{
		return json_decode($this->labels, true);
	}

	/**
	 * @return array
	 */
	public function getHeaders()
	{
		return json_decode($this->headers, true);
	}

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * @return array
	 */
	public function getFrom()
	{
		return json_decode($this->from, true);
	}

	/**
	 * @return string
	 */
	public function getFromName()
	{
		return $this->fromName;
	}

	/**
	 * @return string
	 */
	public function getFromEmail()
	{
		return $this->fromEmail;
	}

	/**
	 * @return mixed
	 */
	public function getTo()
	{
		return json_decode($this->to, true);
	}

	/**
	 * @return string
	 */
	public function getDeliveredTo()
	{
		return $this->deliveredTo;
	}

	/**
	 * @return string
	 */
	public function getPlainTextBody()
	{
		return $this->plainTextBody;
	}

	/**
	 * @return string
	 */
	public function getRawPlainTextBody()
	{
		return $this->rawPlainTextBody;
	}

	/**
	 * @return bool
	 */
	public function getAttachments()
	{
		return $this->attachments == 1 ? true : false;
	}
}
