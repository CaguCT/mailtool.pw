<?php
/**
 * Created by IntelliJ IDEA.
 * User: caguct
 * Date: 8/13/18
 * Time: 11:51 AM
 */

namespace App\Helpers;

class Helper {

	static function viewLabels(array $labels)
	{
		$result = '';

		foreach($labels as $label)
		{
			$result = view('mail.parts.badge', compact('label'));
		}

		return $result;
	}
}
