@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if (session('status'))
                <div class="col-12">
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                </div>
            @endif
            <div class="col-md-3">
                @include('mail.parts.menu')
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $mail->getSubject() }}</h5>
                        <h6 class="card-subtitle mb-2">
                            <b>{{ $mail->getFromName() }}</b> <span
                                    class="text-muted">{{ $mail->getFromEmail() }}</span>
                        </h6>
                        {!! $mail->getHtmlBody() !!}
                        <div class="mt-4">
                            <button type="button" class="btn btn-primary float-lg-right">Send trial</button>
                            <button type="button" class="btn btn-danger">More details</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
